﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTouch : MonoBehaviour {

	private Animator animator;
	private AudioSource univoice;

	public AudioClip voice_01;
	public AudioClip voice_02;


	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator> ();
		univoice = GetComponent<AudioSource> ();

		animator.SetBool ("Touch", false);
		animator.SetBool ("TouchHead", false);	
	}
	
	// Update is called once per frame
	void Update () {
		Ray ray;
		RaycastHit hit;
		GameObject hitobject;

		if (animator.GetCurrentAnimatorStateInfo (0).nameHash == Animator.StringToHash ("Base Layer.Idol"))
			animator.SetBool ("Motion_Idle", true);
		else
			animator.SetBool ("Motion_Idle", false);

		if (Input.GetMouseButtonDown (0))// && animator.GetCurrentAnimatorStateInfo(0).nameHash == Animator.StringToHash("Base Layer.Idol")) 
		{
			ray = Camera.main.ScreenPointToRay (Input.mousePosition);

			if (Physics.Raycast (ray, out hit, 100)) {
				//Debug.Log ("hit");
				hitobject = hit.collider.gameObject;

				if (hitobject.gameObject.tag == "Head") {
					animator.SetBool ("TouchHead", true);

					univoice.clip = voice_01;
					univoice.Play ();

					animator.SetBool ("Face_Happy", true);
					animator.SetBool ("Face_Angry", false);
				}
				else if (hitobject.gameObject.tag == "Breast") {
					animator.SetBool ("Touch", true);

					univoice.clip = voice_02;
					univoice.Play ();

					animator.SetBool ("Face_Happy", false);
					animator.SetBool ("Face_Angry", true);

					displayMsg.setDispMsg ("Oops!!");
				}
			}	
		} 
		else {
			animator.SetBool ("Touch", false);
			animator.SetBool ("TouchHead", false);

			animator.SetBool ("Face_Happy", false);
			animator.SetBool ("Face_Angry", false);
		}

	}
}
