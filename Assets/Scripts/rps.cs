﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rps : MonoBehaviour {
	public GUIStyle btStyleMode;
	public GUIStyle btStyleGoo;
	public GUIStyle btStyleChoki;
	public GUIStyle btStylePar;


	public AudioClip voice_janken_start;
	public AudioClip voice_janken_pon;
	public AudioClip voice_janken_goo;
	public AudioClip voice_janken_choki;
	public AudioClip voice_janken_par;
	public AudioClip voice_janken_win;
	public AudioClip voice_janken_loose;
	public AudioClip voice_janken_draw;

	private Animator animator;
	private AudioSource univoice;

	enum STATE_RPS
	{
		JANKEN = 0,
		GOO,
		CHOKI,
		PAR,
		DRAW,
		WIN,
		LOOSE
	}

	STATE_RPS myHand;
	STATE_RPS oppHand;
	STATE_RPS rpsResult;

	bool flagRps = false;
	int modeRps = 0;

	float waitTime;

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator> ();
		univoice = GetComponent<AudioSource> ();

		myHand = STATE_RPS.JANKEN;
		oppHand = STATE_RPS.JANKEN;
		rpsResult = STATE_RPS.DRAW;

		waitTime = 0.0f;
		initUnityChanAction ();
	}
	
	// Update is called once per frame
	void Update () {
		if (flagRps == true) {
			switch(modeRps)
			{
			case 0:
				UnityChanAction (STATE_RPS.JANKEN);
				modeRps++;
				break;
			case 1:
				initUnityChanAction ();
				break;
			case 2:
				{
					//UnityChanAction (myHand);
					rpsResult = STATE_RPS.DRAW;

					int randRps = Random.Range (0, 3);
					switch (randRps) {
					case 0:
						oppHand = STATE_RPS.GOO;
						break;
					case 1:
						oppHand = STATE_RPS.CHOKI;
						break;
					case 2:
					default:
						oppHand = STATE_RPS.PAR;
						break;
					}
						
					if (myHand == oppHand) {
						rpsResult = STATE_RPS.DRAW;
					} else {
						switch (oppHand) {
						case STATE_RPS.GOO:
							if (myHand == STATE_RPS.PAR) {
								rpsResult = STATE_RPS.LOOSE;
							}
							break;
						case STATE_RPS.CHOKI:
							if (myHand == STATE_RPS.GOO) {
								rpsResult = STATE_RPS.LOOSE;
							}
							break;
						case STATE_RPS.PAR:
							if (myHand == STATE_RPS.CHOKI) {
								rpsResult = STATE_RPS.LOOSE;
							}
							break;
						}

						if (rpsResult != STATE_RPS.LOOSE)
							rpsResult = STATE_RPS.WIN;
					}

					UnityChanAction (oppHand);

					modeRps++;
				}
				break;
			case 3:
				waitTime += Time.deltaTime;

				if (waitTime > 1.5f) {
					waitTime = 0;
					UnityChanAction (rpsResult);

					modeRps++;
				}
					
				break;

			case 4:
				modeRps = 0;
				flagRps = false;
				//initUnityChanAction ();
				break;
				
			default :
				break;
			}
		}
	}

	void OnGUI()
	{
		const float screenWidth = 1200;

		float buttonSize = 200;
		float button0PosX = 10;
		float button1PosX = (screenWidth - buttonSize) / 2 - (buttonSize + 20);
		float button2PosX = (screenWidth - buttonSize) / 2;
		float button3PosX = (screenWidth - buttonSize) / 2 + (buttonSize + 20);
		float buttonPoxY = 450;

		float factorRate = Screen.width / screenWidth;

		buttonSize *= factorRate;
		buttonPoxY *= factorRate;
		button0PosX *= factorRate;
		button1PosX *= factorRate;
		button2PosX *= factorRate;
		button3PosX *= factorRate;

		if (flagRps == false) {
			if (GUI.Button(new Rect(button0PosX, buttonPoxY, buttonSize, buttonSize), "Rock Paper Siccors", btStyleMode))
			{
				flagRps = true;
			}
		}

		if (modeRps == 1) {
			if (GUI.Button(new Rect(button1PosX, buttonPoxY, buttonSize, buttonSize), "Rock", btStyleGoo))
			{
				myHand = STATE_RPS.GOO;
				modeRps++;
			}			

			if (GUI.Button(new Rect(button2PosX, buttonPoxY, buttonSize, buttonSize), "Siccors", btStyleChoki))
			{
				myHand = STATE_RPS.CHOKI;
				modeRps++;
			}	

			if (GUI.Button(new Rect(button3PosX, buttonPoxY, buttonSize, buttonSize), "Paper", btStylePar))
			{
				myHand = STATE_RPS.PAR;
				modeRps++;
			}	

		}

	}

	void UnityChanAction(STATE_RPS action)
	{
		switch(action)
		{
		case STATE_RPS.JANKEN:
			animator.SetBool ("Janken", true);
			univoice.clip = voice_janken_start;
			break;

		case STATE_RPS.GOO:
			animator.SetBool ("Goo", true);
			univoice.clip = voice_janken_goo;
			break;

		case STATE_RPS.CHOKI:
			animator.SetBool ("Choki", true);
			univoice.clip = voice_janken_choki;
			break;

		case STATE_RPS.PAR:
			animator.SetBool ("Par", true);
			univoice.clip = voice_janken_par;
			break;

		case STATE_RPS.DRAW:
			animator.SetBool ("Aiko", true);
			univoice.clip = voice_janken_draw;
			break;

		case STATE_RPS.WIN:
			animator.SetBool ("Win", true);
			univoice.clip = voice_janken_win;
			break;

		case STATE_RPS.LOOSE:
			animator.SetBool ("Loose", true);
			univoice.clip = voice_janken_loose;
			break;

		default:
			univoice.clip = null;
			break;

		}

		if (univoice.clip != null)
			univoice.Play ();
	}

	void initUnityChanAction()
	{
		animator.SetBool ("Janken", false);
		animator.SetBool ("Goo", false);
		animator.SetBool ("Choki", false);
		animator.SetBool ("Par", false);
		animator.SetBool ("Aiko", false);
		animator.SetBool ("Win", false);
		animator.SetBool ("Loose", false);
	}

}
