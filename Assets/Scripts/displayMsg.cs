﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class displayMsg : MonoBehaviour {

	public GUIStyle msgWnd;

	static int lengthMsg = 0;
	static bool flagDisp = false;
	static float waitDspTime = 0;
	static string dispMsg = string.Empty;

	float nextDspTime = 0.01f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (flagDisp == true) {
			if (Time.time > nextDspTime) {
				if (lengthMsg < dispMsg.Length)
					lengthMsg++;
			}

			nextDspTime += 0.01f;

			if (lengthMsg >= dispMsg.Length) {
				waitDspTime += Time.deltaTime;
			}

			if (waitDspTime > dispMsg.Length / 4) {
				flagDisp = false;
			}
		}


	}

	void OnGUI()
	{
		const float screenWidth = 1200;

		float factorRate = Screen.width / screenWidth;
		float msgWidth = 800;
		float msgHeight = 200;
		float msgPosX = (screenWidth - msgWidth) / 2;
		float msgPosY = 450;
		float msgPadding = 22;
		float msgPadding2 = 20;

		msgWidth *= factorRate;
		msgHeight *= factorRate;
		msgPosX *= factorRate;
		msgPosY *= factorRate;
		msgPadding *= factorRate;
		msgPadding2 *= factorRate;

		GUIStyle textStyle = new GUIStyle ();
		textStyle.fontSize = (int)(30 * factorRate);

		if (flagDisp == true) {
			GUI.Box (new Rect (msgPosX, msgPosY, msgWidth, msgHeight), "Message Box", msgWnd);

			//Debug.Log (lengthMsg.ToString());
			textStyle.normal.textColor = Color.black;
			GUI.Label (new Rect (msgPosX + msgPadding, msgPosY + msgPadding, msgWidth - msgPadding * 2, msgHeight - msgPadding * 2),
				dispMsg.Substring(0,lengthMsg), textStyle);

			textStyle.normal.textColor = Color.white;
			GUI.Label (new Rect (msgPosX + msgPadding2, msgPosY + msgPadding2, msgWidth - msgPadding2 * 2, msgHeight - msgPadding2 * 2),
				dispMsg.Substring(0,lengthMsg), textStyle);
		}

	}

	public static void setDispMsg(string msg)
	{
		dispMsg = msg;
		flagDisp = true;
		waitDspTime = 0;
		lengthMsg = 0;
		Debug.Log (dispMsg);
	}
}
